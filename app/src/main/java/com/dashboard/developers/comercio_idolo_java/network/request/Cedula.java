package com.dashboard.developers.comercio_idolo_java.network.request;

/**
 * Created by Developers on 21/12/2017.
 */

public class Cedula {
    String cedula;

    public Cedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
}
