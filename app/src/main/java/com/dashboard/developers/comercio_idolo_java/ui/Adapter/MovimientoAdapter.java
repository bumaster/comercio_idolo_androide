package com.dashboard.developers.comercio_idolo_java.ui.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.model.Summary;

import java.util.List;

/**
 * Created by Developers on 26/11/2017.
 */

public class MovimientoAdapter extends RecyclerView.Adapter<MovimientoAdapter.ViewHolder> {

    private List<Summary> listSummary;

    public MovimientoAdapter(List<Summary> summary) {
        this.listSummary = summary;
    }

    public void add(Summary summary) {
        listSummary.add(summary);
        notifyItemInserted(listSummary.size() - 1);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movimientos_debit, parent, false);
        int layout = -1;
        switch (viewType) {
            case Summary.TYPE_SUMMARY_CREDT:
                layout = R.layout.item_movimientos_credit;
                break;
            case Summary.TYPE_SUMMARY_DEBIT:
                layout = R.layout.item_movimientos_debit;
                break;
        }
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(layout, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Summary summary = listSummary.get(position);

//        String alphabet = summary.getUsername().substring(0, 1);
        holder.txtName.setText(summary.getName());
        holder.txtDescription .setText(summary.getDescription());
        holder.txtAmount.setText(summary.getAmount());
        holder.txtDate.setText(summary.getDate());
    }

    @Override
    public int getItemCount() {
        if (listSummary != null) {
            return listSummary.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return listSummary.get(position).getType();
    }

    public Summary getSummary(int position) {
        return listSummary.get(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtName, txtDescription,txtDate,txtAmount;

        ViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.name);
            txtDescription = (TextView) itemView.findViewById(R.id.description);
            txtDate = (TextView) itemView.findViewById(R.id.date);
            txtAmount = (TextView) itemView.findViewById(R.id.amount);
        }
    }
}
