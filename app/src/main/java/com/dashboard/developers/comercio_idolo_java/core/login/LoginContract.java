package com.dashboard.developers.comercio_idolo_java.core.login;

import android.app.Activity;

import com.dashboard.developers.comercio_idolo_java.network.NetworkError;

/**
 * Created by Developers on 23/10/2017.
 */

public interface LoginContract {
    interface View {
        void onLoginSuccess(String message);

        void onLoginFailure(String message,String tittle);
    }

    interface Presenter{
        void login(Activity activity,String email,String password);
    }

    interface Interactor{
        void perfomanceLoginConection(Activity activity,String email,String password);
    }

    interface OnLoginListener {
        void onSuccess(String message);

        void onFailure(String message);

        void onFailureNetworking(NetworkError networkError);
    }
}
