package com.dashboard.developers.comercio_idolo_java.model;

/**
 * Created by Developers on 30/10/2017.
 */

public class Account {
    private String            number;
    private String            description;
    private String            type;
    private String            typeLabel;
    private String            availableBalance;
    private String            accountingBalance;
    private String            customerName;
    private String            id;

    public Account(String number,
                   String description,
                   String type,
                   String typeLabel,
                   String availableBalance,
                   String accountingBalance,
                   String customerName,
                   String id) {
        this.number = number;
        this.description = description;
        this.type = type;
        this.typeLabel = typeLabel;
        this.availableBalance = availableBalance;
        this.accountingBalance = accountingBalance;
        this.customerName = customerName;
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public String getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(String availableBalance) {
        this.availableBalance = availableBalance;
    }

    public String getAccountingBalance() {
        return accountingBalance;
    }

    public void setAccountingBalance(String accountingBalance) {
        this.accountingBalance = accountingBalance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
