package com.dashboard.developers.comercio_idolo_java.network.request;

/**
 * Created by Developers on 9/11/2017.
 */

public class RequestLogin {
    String username;
    String password;

    public RequestLogin(String username,String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
