package com.dashboard.developers.comercio_idolo_java.core.login;

import android.app.Activity;
import android.util.Log;

import com.dashboard.developers.comercio_idolo_java.network.NetworkError;


public class LoginPresenter implements  LoginContract.Presenter,LoginContract.OnLoginListener {
    private LoginContract.View mLoginView;
    private LoginContract.Interactor mLoginInteractor;

    public LoginPresenter(LoginContract.View loginView) {
        this.mLoginView = loginView;
        mLoginInteractor = new LoginInteractor(this);
    }

    @Override
    public void login(Activity activity, String email, String password) {
        mLoginInteractor.perfomanceLoginConection(activity,email,password);
    }

    @Override
    public void onSuccess(String message) {

        mLoginView.onLoginSuccess(message);
    }

    @Override
    public void onFailure(String message) {
        mLoginView.onLoginFailure(message,"!Proble de accessos!");
    }

    @Override
    public void onFailureNetworking(NetworkError networkError) {
        mLoginView.onLoginFailure(networkError.getAppErrorMessage(),"!Algo no va bien!");
        Log.v("ResponseGetCodeToken", "token: " + networkError.getCode());
    }
}
