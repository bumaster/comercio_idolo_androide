package com.dashboard.developers.comercio_idolo_java.ui.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.model.Cart;
import com.dashboard.developers.comercio_idolo_java.ui.Activity.RecargarPaso1Activity;
import com.dashboard.developers.comercio_idolo_java.ui.Adapter.CartAdapter;

import java.util.ArrayList;

/**
 * Created by Developers on 13/11/2017.
 */

public class FragmentPayment extends Fragment{
    ArrayList<Cart> cart;
    private static CartAdapter cartAdapter;
    ListView listView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_payment, container, false);
        listView=(ListView)fragmentView.findViewById(R.id.list_cart);

        cargatOptions(fragmentView);
        return fragmentView;
    }

    private void cargatOptions(View view) {

        cart= new ArrayList<>();

        cart.add(new Cart("Trasferencia Bancaria", "Android 1.0", "Envia tranferencia a cuenta","September 23, 2008",""));
        cart.add(new Cart("Pago con tarjetas", "Android 1.1", "Paga con cualquier tarjeta de credito o debito","Pago con tarjeta",""));

        cartAdapter = new CartAdapter(getContext(),2,cart);
        listView.setAdapter(cartAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                if(id == 0){
                    RecargarPaso1Activity.startActivity(getContext());                }

            }
        });
    }


}
