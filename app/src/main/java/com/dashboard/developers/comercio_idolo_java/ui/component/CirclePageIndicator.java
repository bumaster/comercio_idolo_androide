package com.dashboard.developers.comercio_idolo_java.ui.component;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.ui.listener.PageIndicator;

/**
 * Created by Developers on 30/10/2017.
 */
@SuppressWarnings("unused")
public class CirclePageIndicator extends View implements PageIndicator {
    private Context context;
    private ViewPager mViewPager;

    public CirclePageIndicator(Context context) {
        this(context, null);
        this.context = context;
    }

    public CirclePageIndicator(Context context, AttributeSet attrs) {
        this(context, attrs,R.attr.vpiCirclePageIndicatorStyle);
    }

    public CirclePageIndicator(Context context, AttributeSet attrs,int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setViewPager(ViewPager view) {
        if (mViewPager == view) {
            return;
        }
        if (mViewPager != null) {
            mViewPager.addOnPageChangeListener(null);
        }
        if (view.getAdapter() == null) {
            throw new IllegalStateException("ViewPager no tiene adaptador instanciado.");
        }
        mViewPager = view;
        mViewPager.addOnPageChangeListener(this);
    }

    @Override
    public void setViewPager(ViewPager view, int initialPosition) {

    }

    @Override
    public void setCurrentItem(int item) {

    }

    @Override
    public void setOnPageChangeListener(ViewPager.OnPageChangeListener listener) {

    }

    @Override
    public void notifyDataSetChanged() {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
