package com.dashboard.developers.comercio_idolo_java.network.response;

/**
 * Created by Developers on 24/10/2017.
 */

public class LoginResponse {
    int code;
    String message;
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
