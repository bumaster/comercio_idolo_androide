package com.dashboard.developers.comercio_idolo_java.network;

import android.text.TextUtils;
import android.util.Log;

import com.dashboard.developers.comercio_idolo_java.utils.ApiError;
import com.dashboard.developers.comercio_idolo_java.utils.ErrorUtils;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import retrofit2.adapter.rxjava.HttpException;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

/**
 * Created by Developers on 4/11/2017.
 */

public class NetworkError extends Throwable {
    public static final String DEFAULT_ERROR_MESSAGE = "¡Algo salió mal! Por favor intente de nuevo.";
    public static final String NETWORK_ERROR_MESSAGE = "Comprueba tu conexion a internet!";
    private static final String ERROR_MESSAGE_HEADER = "Error-Message";
    private static final String ERROR_MESSAGE_TIMEOUT = "El tiempo de espera a culminado, vulve a intentar en unos minutos";
    private static final String ERROR_MESSAGE_CONECCION = "¡Algo salió mal! Por favor intente en unos minutos.";
    private final Throwable error;
    private int code;
    private String message;

    public NetworkError(Throwable e) {
        super(e);
        this.error = e;
    }

    public String getMessage() {
        return this.message;
    }

    public boolean isAuthFailure() {
        return error instanceof HttpException &&
                ((HttpException) error).code() == HTTP_UNAUTHORIZED;
    }

    public boolean isResponseNull() {
        return error instanceof HttpException && ((HttpException) error).response() == null;
    }

    public int getCode() {
        return code;
    }

    public String getAppErrorMessage() {
        Log.v("erroNetworking", String.valueOf(this.error));
        if(this.error instanceof ConnectException) return ERROR_MESSAGE_CONECCION;
        if(this.error instanceof IOException) return NETWORK_ERROR_MESSAGE;
        if(this.error instanceof SocketTimeoutException) return ERROR_MESSAGE_TIMEOUT;
        if(this.error instanceof HttpException){

            retrofit2.Response<?> response = ((HttpException) this.error).response();
            ApiError error = ErrorUtils.parseError(response);
            this.code = error.getCode();
            this.message = error.getMessage();

            Log.v("erroResponseCOdeMessage", String.valueOf(error.getMessage()));
            Log.v("erroResponseCOde", String.valueOf(error.getCode()));

            return error.getMessage();
        }

        return DEFAULT_ERROR_MESSAGE;
    }

    protected String getJsonStringFromResponse(final Response<?> response) {
        try {
            String jsonString = response.errorBody().string();
            Response errorResponse = new Gson().fromJson(jsonString, Response.class);
//            return errorResponse.status;
            return errorResponse.message();
        } catch (Exception e) {
            return null;
        }
    }

    public Throwable getError() {
        return error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NetworkError that = (NetworkError) o;

        return error != null ? error.equals(that.error) : that.error == null;

    }

    @Override
    public int hashCode() {
        return error != null ? error.hashCode() : 0;
    }
}
