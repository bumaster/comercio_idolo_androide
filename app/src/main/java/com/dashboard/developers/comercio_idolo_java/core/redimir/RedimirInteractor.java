package com.dashboard.developers.comercio_idolo_java.core.redimir;

import android.app.Activity;
import android.util.Log;

import com.dashboard.developers.comercio_idolo_java.model.Customer;
import com.dashboard.developers.comercio_idolo_java.network.ApiAdapter;
import com.dashboard.developers.comercio_idolo_java.network.ApiService;
import com.dashboard.developers.comercio_idolo_java.network.NetworkError;
import com.dashboard.developers.comercio_idolo_java.network.request.Cedula;
import com.dashboard.developers.comercio_idolo_java.network.request.RequestRedimir;
import com.dashboard.developers.comercio_idolo_java.network.response.CustomerCedula;
import com.dashboard.developers.comercio_idolo_java.network.response.LoginResponse;
import com.dashboard.developers.comercio_idolo_java.network.response.Response;
import com.dashboard.developers.comercio_idolo_java.network.response.ResponseCedula;

import java.net.SocketTimeoutException;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Developers on 31/10/2017.
 */

public class RedimirInteractor implements RedimirContract.Interactor{

    private RedimirContract.OnRedimeListener rListener;
    private CompositeSubscription subscriptions;

    public RedimirInteractor(RedimirContract.OnRedimeListener rListener) {
        this.rListener = rListener;
        this.subscriptions = new CompositeSubscription();
    }

    @Override
    public void perfomanceRedimeConection(Activity activity, String amount,String cedula) {

        ApiService client = ApiAdapter.createService(ApiService.class);
        RequestRedimir requestRedimir = new RequestRedimir(amount,cedula);
        Subscription subscription =  client.recibir(requestRedimir, Customer.getCustomer().getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer retryCount, Throwable throwable) {
                        return retryCount < 6 && throwable instanceof SocketTimeoutException;
                    }
                })
                .subscribe(new Subscriber<Response>() {

                    @Override
                    public void onCompleted() {
                        Log.v("conComplete",">>>>");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.v("errorOn", String.valueOf(e));
                        rListener.onFailureNetwork(new NetworkError(e));

                    }

                    @Override
                    public void onNext(Response RedimirResponse) {
                        Log.v("onNexr", RedimirResponse.getMessage());
                        rListener.onSuccess(RedimirResponse.getMessage());
                    }
                });

        subscriptions.add(subscription);
    }

    @Override
    public void perfomanceConsultarCedula(Activity activity, String cedula) {
        Cedula ncedula = new Cedula(cedula);
        ApiService client = ApiAdapter.createService(ApiService.class);
        Subscription subscription =  client.searchCedula(Customer.getCustomer().getToken(),ncedula)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer retryCount, Throwable throwable) {
                        return retryCount < 6 && throwable instanceof SocketTimeoutException;
                    }
                })
                .subscribe(new Subscriber<ResponseCedula>() {

                    @Override
                    public void onCompleted() {
                        Log.v("conComplete",">>>>");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.v("errorOnCedula", String.valueOf(e));
                        rListener.onFaildCedula(new NetworkError(e));

                    }

                    @Override
                    public void onNext(ResponseCedula response) {
                        CustomerCedula customerc = response.getCustomer();
                        Log.v("onNexrCedulassss", String.valueOf(customerc.getName()));
                        rListener.onSuccessCedula(customerc.getName());
                    }
                });

        subscriptions.add(subscription);
    }


    @Override
    public void OnSuscription() {
//        subscriptions.unsubscribe();
    }

    @Override
    public void OutSuscription() {
        subscriptions.clear();
    }


}
