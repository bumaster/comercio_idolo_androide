package com.dashboard.developers.comercio_idolo_java.core.redimir;

import android.app.Activity;
import android.util.Log;

import com.dashboard.developers.comercio_idolo_java.network.NetworkError;
import com.dashboard.developers.comercio_idolo_java.network.response.LoginResponse;

/**
 * Created by Developers on 31/10/2017.
 */

public class RedimirPresenter implements RedimirContract.Presenter ,RedimirContract.OnRedimeListener{
    private RedimirContract.View  rView;
    private RedimirContract.Interactor rIteractor;

    public RedimirPresenter(RedimirContract.View view) {
        rView = view;
        rIteractor = new RedimirInteractor(this);
    }

    @Override
    public void redime(Activity activity, String amount,String cedula) {
        rIteractor.perfomanceRedimeConection(activity,amount,cedula);
    }

    @Override
    public void consultarCedula(Activity activity, String cedula) {
        rIteractor.perfomanceConsultarCedula(activity,cedula);
    }

    @Override
    public void onSuccess(String message) {
        rView.onRedimeSuccess(message);
    }


    @Override
    public void onFailureNetwork(NetworkError networkError) {
        rView.onFailureNetwork(networkError.getAppErrorMessage());
        Log.v("ResponseGetCodeToken", "token: " + networkError.getCode());
    }

    @Override
    public void OnSuscription() {

    }

    @Override
    public void OutSuscription() {
        rIteractor.OutSuscription();
    }

    @Override
    public void onSuccessCedula(String name) {
        rView.onSuccessCedula(name);
    }

    @Override
    public void onFaildCedula(NetworkError networkError) {
        networkError.getAppErrorMessage();
        rView.onFaildCedula(networkError.getMessage());
    }
}
