package com.dashboard.developers.comercio_idolo_java.utils;

/**
 * Created by Developers on 25/10/2017.
 */

public class CodeResponseApi {
    private int code;
    private String message;
    public CodeResponseApi(int code) {
        this.code = code;
        this.decodeCode();
    }

    public String getMessage(){
        return this.message;
    }
    private void decodeCode(){
        switch(this.code) {
            case 00:
                this.message =  "TRANSACCION PROCESADA EXITOSAMENTE";
                break;
            case 01:
                this.message =  "TRANSACCION NO DISPONIBLE";
                break;
            case 02:
                this.message =  "RUTEO NO EXISTE";
                break;
            case 03:
                this.message =  "PROCESO NO ACTIVO";
                break;
            case 04:
                this.message =  "PROCESO NO DEFINIDO";
                break;
            case 05:
                this.message =  "AUTORIZADOR NO DEFINIDO";
                break;
            case 06:
                this.message =  "SERVICIO NO DEFINIDO";
                break;
            case 07:
                this.message =  "TIMEOUT EN WEBSERVICE ";
                break;
            case 10:
                this.message =  "REGISTRO YA EXISTE";
                break;
            case 11:
                this.message =  "EXCEPCION EN BASE DE DATOS";
                break;
            case 12:
                this.message =  "USUARIO YA LOGEADO";
                break;
            case 13:
                this.message =  "USUARIO NO LOGEADO";
                break;
            case 14:
                this.message =  "USUARIO BLOQUEADO";
                break;
            case 15:
                this.message =  "USUARIO NO ENCONTRADO";
                break;
            case 16:
                this.message =  "ERROR EN PROCESAMIENTO INTERNO";
                break;
            case 17:
                this.message =  "REQUERIMIENTO DE REVERSO EXITOSO";
                break;
            case 18:
                this.message =  "TRANSACCION NO DISPONIBLE POR EL MOMENTO";
                break;
            case 19:
                this.message =  "ERROR EN EL FORMATO DE MENSAJE";
                break;
            case 20:
                this.message =  "CUENTA DE USUARIO BLOQUEADA";
                break;
            case 21:
                this.message =  "CUENTA CANCELADA";
                break;
            case 22:
                this.message =  "PROBLEMAS DE RECURSOS DEL SISTEMA";
                break;
            case 23:
                this.message =  "REGISTRO DE CUENTA NO EXISTE";
                break;
            case 24:
                this.message =  "ERROR EN CODIFICACION/DECODIFICACION";
                break;
            case 25:
                this.message =  "REGISTRO DE TERMINAL NO EXISTE";
                break;
            case 26:
                this.message =  "REGISTRO DE COMERCIO NO EXISTE";
                break;
            case 27:
                this.message =  "RELACION INCORRECTA (TERMINAL/COMERCIO)";
                break;
            case 28:
                this.message =  "TERMINAL BLOQUEADO";
                break;
            case 29:
                this.message =  "COMERCIO BLOQUEADO";
                break;
            case 30:
                this.message =  "TIEMPO EXCEDIDO EN APROBACION DEL CREDITO";
                break;
            case 31:
                this.message =  "CREDITO PENDIENTE NO SE ENCUENTRA";
                break;
            case 32:
                this.message =  "DEBITO PENDIENTE NO SE ENCUENTRA";
                break;
            case 33:
                this.message =  "TRANSACCION RECHAZADA POR EL USUARIO";
                break;
            case 34:
                this.message =  "CUENTA DE COMERCIO BLOQUEADA";
                break;
            case 35:
                this.message =  "FONDOS INSUFICIENTES EN LA CUENTA DEL COMERCIO";
                break;
            case 36:
                this.message =  "FONDOS INSUFICIENTES EN LA CUENTA DEL USUARIO";
                break;
            case 37:
                this.message =  "EXISTE UNA SOLICITUD PENDIENTE DE APROBACION";
                break;
            case 38:
                this.message =  "TIEMPO EXCEDIDO EN APROBACION DE COMPRA";
                break;
            case 39:
                this.message =  "CLAVE INCORRECTA";
                break;
            case 44:
                this.message =  "Usuario debe realizar cambio de clave";
                break;
            case 1010:
                this.message =  "La transaccion fue exitosa";
                break;
            case 1011:
                this.message =  "Error de coneccion con servidor, vuelva a intentar en unos minustos";
                break;
            case 1012:
                this.message =  "Error de coneccion con servidor, vuelva a intentar en unos minustos";
                break;
            case 1013:
                this.message =  "Ups al parecer los datos son incorrectos, vuelva a intentar";
                break;
            case 1014:
                this.message =  "Error de Authorizacion";
                break;
            default:
                this.message =  "Codigo no identificao >" + this.code;
        }
    }


}
