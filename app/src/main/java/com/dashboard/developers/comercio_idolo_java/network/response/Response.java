package com.dashboard.developers.comercio_idolo_java.network.response;

/**
 * Created by Developers on 8/11/2017.
 */

public class Response {
    int code;
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
