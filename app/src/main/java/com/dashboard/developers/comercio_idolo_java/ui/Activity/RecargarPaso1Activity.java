package com.dashboard.developers.comercio_idolo_java.ui.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dashboard.developers.comercio_idolo_java.R;

public class RecargarPaso1Activity extends AppCompatActivity {

    private BottomSheetBehavior mBottomSheetBehavior;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, RecargarPaso1Activity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paso1_recargar);

        View bottomSheet = findViewById( R.id.bottom_sheet );
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(0);
    }
}
