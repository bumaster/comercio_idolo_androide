package com.dashboard.developers.comercio_idolo_java.core.generar;

import android.app.Activity;

import com.dashboard.developers.comercio_idolo_java.network.NetworkError;

/**
 * Created by Developers on 7/11/2017.
 */

public class GenerarPresenter implements GenerarContract.Presenter,GenerarContract.OnGenerarListener {

    GenerarContract.View viewGenerar;
    GenerarContract.Interactor interactorGenerar;

    public GenerarPresenter(GenerarContract.View view) {
        this.viewGenerar = view;
        interactorGenerar = new GenerarInteractor(this);
    }

    @Override
    public void generar(Activity activity,String amount,String cedula,String amount_transaction,String transaction_code) {
        interactorGenerar.perfomanceGenerarConection(activity,amount,cedula,amount_transaction,transaction_code);
    }

    @Override
    public void consultarCedula(Activity activity, String cedula) {
        interactorGenerar.perfomanceConsultarCedula(activity,cedula);
    }

    @Override
    public void onSuccess(String message) {
        viewGenerar.onGenerarSuccess(message);
    }

    @Override
    public void onFailure(String message) {
        viewGenerar.onGenerarFailure(message);
    }

    @Override
    public void onFailureNetworking(NetworkError networkError) {
        viewGenerar.onGenerarFailure(networkError.getAppErrorMessage());
    }

    @Override
    public void OnSuscription() {

    }

    @Override
    public void OutSuscription() {
        interactorGenerar.OutSuscription();
    }

    @Override
    public void onSuccessCedula(String name) {
        viewGenerar.onSuccessCedula(name);
    }

    @Override
    public void onFaildCedula(NetworkError networkError) {
        networkError.getAppErrorMessage();
        viewGenerar.onFaildCedula(networkError.getMessage());
    }
}
