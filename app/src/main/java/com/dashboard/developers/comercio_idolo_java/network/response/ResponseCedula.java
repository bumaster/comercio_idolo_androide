package com.dashboard.developers.comercio_idolo_java.network.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Developers on 21/12/2017.
 */

public class ResponseCedula {
    String code;
    String message;

    @SerializedName("customer")
    CustomerCedula customer;

    public ResponseCedula(String code, String message, CustomerCedula customer) {
        this.code = code;
        this.message = message;
        this.customer = customer;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public CustomerCedula getCustomer() {
        return customer;
    }
}
