package com.dashboard.developers.comercio_idolo_java.core.generar;

import android.app.Activity;

import com.dashboard.developers.comercio_idolo_java.network.NetworkError;

/**
 * Created by Developers on 7/11/2017.
 */

public interface GenerarContract {

    interface  View {
        void onGenerarSuccess(String message);

        void onGenerarFailure(String message);

        void onFaildCedula(String message);

        void onSuccessCedula(String name);
    }
    interface Presenter{
        void generar(Activity activity,String amount,String cedula,String amount_transaction,String transaction_code);

        void consultarCedula(Activity activity,String cedula);
    }
    interface Interactor{
        void perfomanceGenerarConection(Activity activity,String amount,String cedula,String amount_transaction,String transaction_code);

        void perfomanceConsultarCedula(Activity activity,String cedula);

        void OnSuscription();

        void OutSuscription();
    }
    interface OnGenerarListener{

        void onSuccess(String message);

        void onFailure(String message);

        void onFailureNetworking(NetworkError networkError);

        void OnSuscription();

        void OutSuscription();

        void onSuccessCedula(String name);

        void onFaildCedula(NetworkError networkError);
    }
}
