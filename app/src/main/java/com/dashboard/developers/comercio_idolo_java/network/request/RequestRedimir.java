package com.dashboard.developers.comercio_idolo_java.network.request;

/**
 * Created by Developers on 6/11/2017.
 */

public class RequestRedimir {
    String amount;
    String cedula;

    public RequestRedimir(String amount, String cedula) {
        this.amount = amount;
        this.cedula = cedula;
    }
}
