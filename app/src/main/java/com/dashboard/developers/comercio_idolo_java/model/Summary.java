package com.dashboard.developers.comercio_idolo_java.model;

/**
 * Created by Developers on 26/11/2017.
 */

public class Summary {

    public static final int TYPE_SUMMARY_DEBIT = 0;
    public static final int TYPE_SUMMARY_CREDT = 1;
    public static final int TYPE_LOG = 0;


    String name;
    int type;
    String description;
    String amount;
    String date;

    public Summary() {
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }
    public int getType(){
        return type;
    }

    public static class Builder{
        private final int mType;
        private String description;
        private String amount;
        private String date;
        private String name;

        public Builder(int type){
            mType = type;
        }

        public Builder name(String name){
            this.name = name;
            return  this;
        }

        public Builder amount(String amount){
            this.amount = amount;
            return this;
        }

        public Builder description(String descrip){
            this.description = descrip;
            return this;
        }
        public Builder date(String date){
            this.date = date;
            return this;
        }

        public Summary build(){
            Summary summary = new Summary();
            summary.type =this.mType;
            summary.name = this.name;
            summary.amount = this.amount;
            summary.date = this.date;
            summary.description = this.description;
            return summary;
        }
    }
}
