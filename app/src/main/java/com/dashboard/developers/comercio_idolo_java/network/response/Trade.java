package com.dashboard.developers.comercio_idolo_java.network.response;

/**
 * Created by Developers on 9/11/2017.
 */

public class Trade {
    String amount;
    String available_balance;
    String debit_amount;
    String credit_amount;

    public Trade(String amount, String available_balance, String debit_amount, String credit_amount) {
        this.amount = amount;
        this.available_balance = available_balance;
        this.debit_amount = debit_amount;
        this.credit_amount = credit_amount;
    }

    public String getAmount() {
        return amount;
    }
    public String getAvailable_balance() {
        return available_balance;
    }
    public String getDebit_amount() { return debit_amount; }
    public String getCredit_amount() {return credit_amount;  }

}
