package com.dashboard.developers.comercio_idolo_java.ui.Fragments;

import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.core.generar.GenerarContract;
import com.dashboard.developers.comercio_idolo_java.core.generar.GenerarPresenter;
import com.dashboard.developers.comercio_idolo_java.utils.Generarpromedio;

/**
 * Created by Developers on 3/11/2017.
 */

public class FragmentGenerar extends Fragment implements GenerarContract.View{

    private GenerarPresenter gPresenter;
    private EditText value;
    private TextView valorGenerar;
    private String amount;
    private MaterialDialog progressDialog;
    private MaterialDialog progressDialogResponse;
    private TextView _numberCedula;
    private TextView _numberFactura;
    private MaterialDialog cuestionSure;
    private String amount_transaction;
    private TextView _transaction_code;
    private TextView textViewCedula;
    private boolean isCedula = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View fragmentView = inflater.inflate(R.layout.fragment_generar, container, false);

        chargeElements(fragmentView);
        bindView(fragmentView);
        return fragmentView;
    }

    private void chargeElements(View fragmentView) {
        value = (EditText) fragmentView.findViewById(R.id.payValue);
        valorGenerar = (TextView) fragmentView.findViewById(R.id.valueGolesGenerar);

        _numberCedula = (TextView) fragmentView.findViewById(R.id.numberCedula);
        _numberFactura = (TextView) fragmentView.findViewById(R.id.numberFacture);
        _transaction_code = (TextView) fragmentView.findViewById(R.id.transaction_code);
        textViewCedula = (TextView) fragmentView.findViewById(R.id.textViewCedula);

    }

    private void bindView(View fragmentView) {

        value.setRawInputType(Configuration.KEYBOARD_12KEY);
        value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int i, int i1, int i2) {
                StringBuilder cashAmountBuilder = new StringBuilder(s.toString());
                Selection.setSelection(value.getText(), cashAmountBuilder.toString()
                        .length());
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (!s.toString().matches("(\\d{1,3}(\\,\\d{3})*|(\\d+))(\\.\\d{2})?$")) {
                    String userInput = "" + s.toString().replaceAll("[^\\d]", "");
                    StringBuilder cashAmountBuilder = new StringBuilder(userInput);

                    while (cashAmountBuilder.length() > 3
                            && cashAmountBuilder.charAt(0) == '0') {
                        cashAmountBuilder.deleteCharAt(0);
                    }
                    while (cashAmountBuilder.length() < 3) {
                        cashAmountBuilder.insert(0, '0');
                    }
                    cashAmountBuilder.insert(cashAmountBuilder.length() - 2, '.');

                    value.setText(cashAmountBuilder.toString());
                    // keeps the cursor always to the right
                    Selection.setSelection(value.getText(), cashAmountBuilder.toString()
                            .length());

                    Float d = new Float(cashAmountBuilder.toString());

                    Generarpromedio valuerR = new Generarpromedio(d);
                    valorGenerar.setText(valuerR.gerPromedioRecibirString());

                    //setear valor que va a ser enviado al presentador
                    amount = valuerR.gerPromedioRecibirString();
                    amount_transaction = cashAmountBuilder.toString();

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.v("afterTexCanged",">>>>>");
            }
        });

        _numberCedula.setRawInputType(Configuration.KEYBOARD_12KEY);
        _numberCedula.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() > 9 && editable.length() < 11){
                    Log.v("generaoituype",editable.toString());
                    gPresenter.consultarCedula(getActivity(),editable.toString());
                    _numberCedula.setEnabled(false);
                    isCedula = true;
                }else if(editable.length() <= 9){
                    _numberCedula.setError("Numero de cedula no verificado");
                    textViewCedula.setText("Ingresa el numero de celuda del usuario");
                    isCedula = false;
                }
            }
        });
    }

    public boolean validate() {
        boolean valid = true;
        String numberCedula = _numberCedula.getText().toString();
        String numberFactura = _numberFactura.getText().toString();
        String transaction_code = _transaction_code.getText().toString();

        if (numberCedula.isEmpty() || numberCedula.length() < 10 || numberCedula.length() > 10) {
            _numberCedula.setError("minimo de caracteres 10");
            valid = false;
        } else {
            _numberCedula.setError(null);
        }
        if (numberFactura.isEmpty() || numberFactura.length() < 3) {
            _numberFactura.setError("minimo de caracteres 3");
            valid = false;
        } else {
            _numberFactura.setError(null);
        }
        if (transaction_code.isEmpty() || transaction_code.length() < 6) {
            _transaction_code.setError("minimo de caracteres 6");
            valid = false;
        } else {
            _transaction_code.setError(null);
        }

        return valid;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        super.onResume();
        init();

    }

    private void init() {
        gPresenter = new GenerarPresenter(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //validar campos
        if (!validate()) {
            return false;
        }

        switch (item.getItemId()) {
            case R.id.action_continuar:
                consultaryCargar();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void consultaryCargar() {
        cuestionSure = new MaterialDialog.Builder(getActivity())
                .title("Estas seguro?")
                .content("Comprueba los valores ingresados si es necesario")
                .positiveText("Aceptar")
                .negativeText("Cancelar")
                .cancelable(false)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Log.v("oncancel","oncancel");
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //crear dialogo
                        progressDialog = new MaterialDialog.Builder(getActivity())
                                .content("Cargando...")
                                .cancelable(false)
                                .progress(true, 0)
                                .negativeText("Cancelar")
                                .cancelListener(new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialog) {
                                        gPresenter.OutSuscription();
                                    }
                                })
                                .show();
                        // ejecutar generar
                        gPresenter.generar(getActivity(),amount,_numberCedula.getText().toString(),amount_transaction,_transaction_code.getText().toString());
                    }
                })
                .show();
    }


    private void creatDialogSucceesOrFalse(String message){
        progressDialogResponse = new MaterialDialog.Builder(getActivity())
                .content(message)
                .cancelable(false)
                .positiveText("Aceptar")
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                    }
                })
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                    }
                })
                .show();
    }

    @Override
    public void onGenerarSuccess(String message) {
        progressDialog.dismiss();
        creatDialogSucceesOrFalse(message);
    }

    @Override
    public void onGenerarFailure(String message) {
        progressDialog.dismiss();
        creatDialogSucceesOrFalse(message);
    }

    @Override
    public void onFaildCedula(String message) {
        _numberCedula.setEnabled(true);
        textViewCedula.setTextColor(Color.parseColor("#ef1b0b"));
        textViewCedula.setText(message);
    }

    @Override
    public void onSuccessCedula(String name) {
        _numberCedula.setEnabled(true);
        isCedula = true;
        textViewCedula.setTextColor(Color.parseColor("#69c603"));
        textViewCedula.setText(name);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.v("onStop","se ejecuto");
        if(progressDialog != null) progressDialog.dismiss();

        gPresenter.OutSuscription();
    }
}
