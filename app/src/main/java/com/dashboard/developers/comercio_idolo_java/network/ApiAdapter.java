package com.dashboard.developers.comercio_idolo_java.network;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiAdapter {
//    public static final String HOST_API = "http://192.207.67.109:8080";   // *--> servidor Qbit
    public static final String HOST_API = "http://192.207.67.109:8080";  // *--> qbit
//      public static final String HOST_API = "http://192.168.1.113:8080";  // *-->house2


    private static final String API_FOLDER = "/api/";
    private static final String BASE_URL   = HOST_API + API_FOLDER;
    private static ApiService api_service;
    private static Retrofit retrofit;
    private String tokenDefaul = "comercio";

    private static OkHttpClient httpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();

                Request.Builder builder = originalRequest.newBuilder()
                                            .header("Content-Type","application/x-www-form-urlencoded");

                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).build();

    public static <S> S createService(Class<S> serviceClass) {
        httpClient = new OkHttpClient.Builder()
                .connectTimeout(19000, TimeUnit.MILLISECONDS)
                .writeTimeout(19000, TimeUnit.MILLISECONDS)
                .readTimeout(20000, TimeUnit.MILLISECONDS)
                .build();
        retrofit = builder.client(httpClient)
                .baseUrl(BASE_URL)
                .build();
        return retrofit.create(serviceClass);
    }

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create());

    public static Retrofit getRetrofit() {
        return retrofit;
    }

}
