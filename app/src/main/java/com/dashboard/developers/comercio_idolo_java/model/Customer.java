package com.dashboard.developers.comercio_idolo_java.model;

/**
 * Created by Developers on 8/11/2017.
 */

public class Customer {
    private String  username;
    private String  token;
    private boolean authentication = false;
    private boolean userRegistered;
    private String balance;
    private String debit_amount;
    private String credit_amount;

    public String getDebit_amount() {
        return debit_amount;
    }

    public void setDebit_amount(String debit_amount) {
        this.debit_amount = debit_amount;
    }

    public String getCredit_amount() {
        return credit_amount;
    }

    public void setCredit_amount(String credit_amount) {
        this.credit_amount = credit_amount;
    }

    public String getBalance() {
        if(this.balance == null){
            return "00.00";
        }
        return balance;
    }

    // Singleton variable and methods
    public static Customer customer;

    public static Customer _instance() {
        if (customer == null)
            customer = new Customer();
        return customer;
    }
    // Destroy singleton.
    public void _reset() {
        customer = null;
    }
    // Singleton definition constructor.
    private Customer() {
        super();
    }

    public void setToken(String token) {
        if(token != null){
            this.authentication = true;
            this.token = token;
        }
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }

    public boolean isAuthentication() {
        return authentication;
    }

    public boolean isUserRegistered() {
        return userRegistered;
    }

    public static Customer getCustomer() {
        return customer;
    }
}
