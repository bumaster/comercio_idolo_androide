package com.dashboard.developers.comercio_idolo_java.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.event.PushNotificationEvent;
import com.dashboard.developers.comercio_idolo_java.ui.Activity.MainActivity;
import com.dashboard.developers.comercio_idolo_java.utils.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by Developers on 14/12/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private  PendingIntent pendingIntent;
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "remoteMessage: " + remoteMessage.getData());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            String title = remoteMessage.getData().get("title");
            String message = remoteMessage.getData().get("text");
            String username = remoteMessage.getData().get("username");
            String uid = remoteMessage.getData().get("uid");
            String fcmToken = remoteMessage.getData().get("fcm_token");

//             Don't show notification if chat activity is open.
//            if (!FirebaseChatMainApp.isChatActivityOpen()) {
                EmitNotification(title,
                        message,
                        username,
                        uid,
                        fcmToken);
//            } else {
//                EventBus.getDefault().post(new PushNotificationEvent(title,
//                        message,
//                        username,
//                        uid,
//                        fcmToken));
//            }
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private void EmitNotification(String title,
                                  String message,
                                  String receiver,
                                  String receiverUid,
                                  String firebaseToken) {
        Log.v("messageNotification","sdfdfsfsdf");
        int badgeCount = 1;
        ShortcutBadger.applyCount(this, badgeCount); //for 1.1.4+

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationID = 1;
        String CHANNEL_ID = "comercio_idolo";
// Set a message count to associate with this notification in the long-press menu.
        int messageCount = 3;
// Create a notification and set a number to associate with it.
        Notification notification =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle("New Messages")
                        .setContentText("You've received 3 new messages.")
                        .setSmallIcon(R.drawable.ic_person)
                        .setNumber(messageCount)
                        .build();
// Issue the notification.
        mNotificationManager.notify(notificationID, notification);
    }
}
