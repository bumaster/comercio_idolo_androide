package com.dashboard.developers.comercio_idolo_java.core.resumen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import com.dashboard.developers.comercio_idolo_java.model.Customer;
import com.dashboard.developers.comercio_idolo_java.model.CustomerMovimientos;
import com.dashboard.developers.comercio_idolo_java.model.MovimientosResponse;
import com.dashboard.developers.comercio_idolo_java.model.Trasactions;
import com.dashboard.developers.comercio_idolo_java.network.ApiAdapter;
import com.dashboard.developers.comercio_idolo_java.network.ApiService;
import com.dashboard.developers.comercio_idolo_java.network.NetworkError;
import com.dashboard.developers.comercio_idolo_java.network.response.BalanceResponse;
import com.dashboard.developers.comercio_idolo_java.network.response.BalanceTotal;
import com.dashboard.developers.comercio_idolo_java.network.response.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Developers on 9/11/2017.
 */

public class ResumenInteractor implements ResumenContract.Interactor {

    private ResumenContract.OnResumen onResumen;
    private CompositeSubscription subscriptions;


    public ResumenInteractor(ResumenContract.OnResumen onResumen) {
        this.onResumen = onResumen;
        this.subscriptions = new CompositeSubscription();
    }

    @Override
    public void perfomanceResumenBalance(Activity activity) {
        Log.v("action","actionRer:" + Customer.getCustomer().getToken());
        ApiService client = ApiAdapter.createService(ApiService.class);
        Subscription subscriptionBalance =  client.resumen(Customer.getCustomer().getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer retryCount, Throwable throwable) {
                        return retryCount < 6 && throwable instanceof SocketTimeoutException;
                    }
                })
                .subscribe(new Subscriber<BalanceResponse>() {

                    @Override
                    public void onCompleted() {
                        Log.v("conComplete",">>>>");
                    }

                    @Override
                    public void onError(Throwable e) {
                        onResumen.onFailureNetwork(new NetworkError(e));
                    }

                    @Override
                    public void onNext(BalanceResponse balanceResponse) {

                        Log.v("customerBalandeMessage", String.valueOf(balanceResponse.getCustomer().getAmount()));
                        onResumen.onSuccessBalance(balanceResponse.getCustomer());
                    }
                });

        subscriptions.add(subscriptionBalance);
    }

    @Override
    public void perfomanceResumenDebit(Activity activity) {
        ApiService client = ApiAdapter.createService(ApiService.class);
        Subscription subscriptionTotalTransactions =  client.totalGenerar(Customer.getCustomer().getToken(),"0")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer retryCount, Throwable throwable) {
                        return retryCount < 6 && throwable instanceof SocketTimeoutException;
                    }
                })
                .subscribe(new Subscriber<MovimientosResponse>() {

                    @Override
                    public void onCompleted() {
                        Log.v("conComplete",">>>>");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.v("perfomancedebit", String.valueOf(e));
                        onResumen.onFailureNetwork(new NetworkError(e));
                    }

                    @SuppressLint("NewApi")
                    @Override
                    public void onNext(MovimientosResponse movimientosResponse) {
//                        CustomerMovimientos cMovimientos = movimientosResponse.getCustomer().getTransactions();

                        Log.v("movimientos", String.valueOf(movimientosResponse.getCustomer().getName()));
                        Log.v("movimientos2333", String.valueOf(movimientosResponse.getTransactions()));
                        if(movimientosResponse.getTransactions().size() > 0){
                            onResumen.onSuccessDebit(movimientosResponse.getTransactions());
                        }
                    }
                });

        subscriptions.add(subscriptionTotalTransactions);
    }

    @Override
    public void perfomanceResumenCredit(Activity activity) {
        ApiService client = ApiAdapter.createService(ApiService.class);
        Subscription subscriptionTotalTransactions =  client.totalGenerar(Customer.getCustomer().getToken(),"1")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer retryCount, Throwable throwable) {
                        return retryCount < 6 && throwable instanceof SocketTimeoutException;
                    }
                })
                .subscribe(new Subscriber<MovimientosResponse>() {

                    @Override
                    public void onCompleted() {
                        Log.v("conComplete",">>>>");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.v("perfomancedebit", String.valueOf(e));
                        onResumen.onFailureNetwork(new NetworkError(e));
                    }

                    @SuppressLint("NewApi")
                    @Override
                    public void onNext(MovimientosResponse movimientosResponse) {
//                        CustomerMovimientos cMovimientos = movimientosResponse.getCustomer().getTransactions();

                        Log.v("movimientos", String.valueOf(movimientosResponse.getCustomer().getName()));
                        Log.v("movimientos2333", String.valueOf(movimientosResponse.getTransactions().get(0).getCom()));
                        if(movimientosResponse.getTransactions().size() > 0){
                            onResumen.onSuccessCredit(movimientosResponse.getTransactions());
                        }
                    }
                });

        subscriptions.add(subscriptionTotalTransactions);
    }

    @Override
    public void perfomanceResumenTotal(Activity activity) {
        ApiService client = ApiAdapter.createService(ApiService.class);
        Subscription subscriptionTotalTransactions =  client.totalTransactions(Customer.getCustomer().getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer retryCount, Throwable throwable) {
                        return retryCount < 6 && throwable instanceof SocketTimeoutException;
                    }
                })
                .subscribe(new Subscriber<BalanceTotal>() {

                    @Override
                    public void onCompleted() {
                        Log.v("conComplete",">>>>");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.v("perfomanceResumenTotal", String.valueOf(e));
                        onResumen.onFailureNetwork(new NetworkError(e));
                    }

                    @Override
                    public void onNext(BalanceTotal response) {

                        Log.v("perfomanceResumenTotal", String.valueOf(response.getCustomer()));
                        onResumen.onSuccessTotal(response.getCustomer());
                    }
                });

        subscriptions.add(subscriptionTotalTransactions);
    }
}
