package com.dashboard.developers.comercio_idolo_java.core.resumen;

import android.app.Activity;

import com.dashboard.developers.comercio_idolo_java.model.Trasactions;
import com.dashboard.developers.comercio_idolo_java.network.NetworkError;
import com.dashboard.developers.comercio_idolo_java.network.response.Trade;

import java.util.List;


/**
 * Created by Developers on 9/11/2017.
 */

public class ResumenPresenter implements ResumenContract.Presenter,ResumenContract.OnResumen {
    private ResumenContract.Interactor iResumen;
    private ResumenContract.View vResumen;
    private com.dashboard.developers.comercio_idolo_java.model.Customer Custom;

    public ResumenPresenter(ResumenContract.View view) {
        this.iResumen = new ResumenInteractor(this);
        this.vResumen = view;
    }

    @Override
    public void resumen(Activity activity) {
        iResumen.perfomanceResumenBalance(activity);
        iResumen.perfomanceResumenTotal(activity);
        iResumen.perfomanceResumenDebit(activity);
        iResumen.perfomanceResumenCredit(activity);
    }

    @Override
    public void onSuccessBalance(Trade customer) {
        vResumen.chargeAmount(customer.getAmount());
    }

    @Override
    public void onSuccessTotal(Trade customer) {
        Custom._instance().setCredit_amount(customer.getCredit_amount());
        Custom._instance().setDebit_amount(customer.getDebit_amount());
        vResumen.onSuccessTotal(customer.getDebit_amount(),customer.getCredit_amount());
    }


    @Override
    public void onFailureNetwork(NetworkError networkError) {
        networkError.getAppErrorMessage();
        if(networkError.getCode()== 1024){
            vResumen.onFailureTokenExpired("Session ha expirado");
        }
    }

    @Override
    public void onSuccessDebit(List<Trasactions> trasactions) {
        vResumen.onSuccessDebit(trasactions);
    }

    @Override
    public void onSuccessCredit(List<Trasactions> trasactions) {
        vResumen.onSuccessCredit(trasactions);
    }
}
