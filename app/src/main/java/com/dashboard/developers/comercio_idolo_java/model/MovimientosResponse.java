package com.dashboard.developers.comercio_idolo_java.model;

import java.util.List;

/**
 * Created by Developers on 27/11/2017.
 */

public class MovimientosResponse {
    int code;
    String message;
    CustomerMovimientos customer;
    List<Trasactions> transactions;

    public List<Trasactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Trasactions> trasactions) {
        this.transactions = trasactions;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CustomerMovimientos getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerMovimientos customer) {
        this.customer = customer;
    }
}
