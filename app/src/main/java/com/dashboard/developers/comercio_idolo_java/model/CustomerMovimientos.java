package com.dashboard.developers.comercio_idolo_java.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Developers on 27/11/2017.
 */

public class CustomerMovimientos {
    String name;
    String amount;

    public String getName() {
        return name;
    }

    public String getAmount() {
        return amount;
    }
}
