package com.dashboard.developers.comercio_idolo_java.core.generar;

import android.app.Activity;
import android.util.Log;

import com.dashboard.developers.comercio_idolo_java.model.Customer;
import com.dashboard.developers.comercio_idolo_java.network.ApiAdapter;
import com.dashboard.developers.comercio_idolo_java.network.ApiService;
import com.dashboard.developers.comercio_idolo_java.network.NetworkError;
import com.dashboard.developers.comercio_idolo_java.network.request.Cedula;
import com.dashboard.developers.comercio_idolo_java.network.request.RequestGenerar;
import com.dashboard.developers.comercio_idolo_java.network.response.CustomerCedula;
import com.dashboard.developers.comercio_idolo_java.network.response.Response;
import com.dashboard.developers.comercio_idolo_java.network.response.ResponseCedula;
import java.net.SocketTimeoutException;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Developers on 7/11/2017.
 */

public class GenerarInteractor implements GenerarContract.Interactor {

    GenerarContract.OnGenerarListener presenterGenerarListener;
    private CompositeSubscription subscriptions;


    public GenerarInteractor(GenerarContract.OnGenerarListener onGenerarListener) {
        this.presenterGenerarListener = onGenerarListener;
        this.subscriptions = new CompositeSubscription();

    }

    @Override
    public void perfomanceGenerarConection(final Activity activity,String amount,String cedula,String amount_transaction,String transaction_code) {

        ApiService client = ApiAdapter.createService(ApiService.class);
        RequestGenerar requestGenerar = new RequestGenerar(amount,cedula,amount_transaction,transaction_code);

        Subscription subscription =  client.generar(requestGenerar, Customer.getCustomer().getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer retryCount, Throwable throwable) {
                        return retryCount < 6 && throwable instanceof SocketTimeoutException;
                    }
                })
                .subscribe(new Subscriber<Response>() {

                    @Override
                    public void onCompleted() {
                        Log.v("conComplete",">>>>");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.v("errorOn", String.valueOf(e));
                        presenterGenerarListener.onFailureNetworking(new NetworkError(e));

                    }

                    @Override
                    public void onNext(Response GenerarResponse) {
                        Log.v("onNexr", GenerarResponse.getMessage());
                        presenterGenerarListener.onSuccess(GenerarResponse.getMessage());
                    }
                });

        subscriptions.add(subscription);

    }

    @Override
    public void perfomanceConsultarCedula(Activity activity, String cedula) {
        Cedula ncedula = new Cedula(cedula);
        ApiService client = ApiAdapter.createService(ApiService.class);
        Subscription subscription =  client.searchCedula(Customer.getCustomer().getToken(),ncedula)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer retryCount, Throwable throwable) {
                        return retryCount < 6 && throwable instanceof SocketTimeoutException;
                    }
                })
                .subscribe(new Subscriber<ResponseCedula>() {

                    @Override
                    public void onCompleted() {
                        Log.v("conComplete",">>>>");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.v("errorOnCedula", String.valueOf(e));
                        presenterGenerarListener.onFaildCedula(new NetworkError(e));

                    }

                    @Override
                    public void onNext(ResponseCedula response) {
                        CustomerCedula customerc = response.getCustomer();
                        Log.v("onNexrCedulassss", String.valueOf(customerc.getName()));
                        presenterGenerarListener.onSuccessCedula(customerc.getName());
                    }
                });

        subscriptions.add(subscription);

    }

    @Override
    public void OnSuscription() {
        // subscriptions.unsubscribe();
    }

    @Override
    public void OutSuscription() {
        subscriptions.clear();
    }
}
