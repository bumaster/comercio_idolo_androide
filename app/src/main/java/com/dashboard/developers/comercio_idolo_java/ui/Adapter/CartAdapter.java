package com.dashboard.developers.comercio_idolo_java.ui.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.model.Cart;

import java.util.List;

/**
 * Created by Developers on 13/11/2017.
 */

public class CartAdapter extends ArrayAdapter<Cart> {
    List<Cart> cart;
    Context context;
    private int lastPosition = -1;
    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtType;
        TextView txtDescription;
        ImageView img;
    }

    public CartAdapter(@NonNull Context context, int resource, @NonNull List<Cart> cart) {
        super(context, R.layout.cart_item, cart);
        this.cart = cart;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Cart dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.cart_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.txtType = (TextView) convertView.findViewById(R.id.type);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }


        viewHolder.txtName.setText(dataModel.getName());
        viewHolder.txtType.setText(dataModel.getDescription());
        // Return the completed view to render on screen
        return convertView;
    }
}
