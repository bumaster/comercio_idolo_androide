package com.dashboard.developers.comercio_idolo_java.core.resumen;

import android.app.Activity;

import com.dashboard.developers.comercio_idolo_java.model.Trasactions;
import com.dashboard.developers.comercio_idolo_java.network.NetworkError;
import com.dashboard.developers.comercio_idolo_java.network.response.Trade;

import java.util.List;

/**
 * Created by Developers on 9/11/2017.
 */

public interface ResumenContract {
    interface View{
        void onResumenSuccess(String message);

        void onResumenFailure(String message);

        void onResumenNetwork(String message);

        void onFailureTokenExpired(String message);

        void chargeAmount(String amount);

        void onSuccessTotal(String debit_amount,String credit_amount);

        void onSuccessDebit(List<Trasactions> trasactions);

        void onSuccessCredit(List<Trasactions> trasactions);
    }

    interface Presenter{
        void resumen(Activity activity);
    }

    interface Interactor{
        void perfomanceResumenTotal(Activity activity);
        void perfomanceResumenBalance(Activity activity);
        void perfomanceResumenDebit(Activity activity);
        void perfomanceResumenCredit(Activity activity);
    }

    interface OnResumen{
        void onSuccessBalance(Trade customer);

        void onSuccessTotal(Trade customer);

        void onFailureNetwork(NetworkError networkError);

        void onSuccessDebit(List<Trasactions> trasactions);

        void onSuccessCredit(List<Trasactions> trasactions);
    }
}
