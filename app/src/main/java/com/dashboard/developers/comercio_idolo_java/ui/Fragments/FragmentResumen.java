package com.dashboard.developers.comercio_idolo_java.ui.Fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.core.resumen.ResumenContract;
import com.dashboard.developers.comercio_idolo_java.core.resumen.ResumenPresenter;
import com.dashboard.developers.comercio_idolo_java.event.PushNotificationEvent;
import com.dashboard.developers.comercio_idolo_java.model.Account;
import com.dashboard.developers.comercio_idolo_java.model.Customer;
import com.dashboard.developers.comercio_idolo_java.model.ResumenTotal;
import com.dashboard.developers.comercio_idolo_java.model.Summary;
import com.dashboard.developers.comercio_idolo_java.model.Trasactions;
import com.dashboard.developers.comercio_idolo_java.network.response.Trade;
import com.dashboard.developers.comercio_idolo_java.ui.Activity.LoginActivity;
import com.dashboard.developers.comercio_idolo_java.ui.Adapter.CustomAndroidGridViewAdapter;
import com.dashboard.developers.comercio_idolo_java.ui.Adapter.MovimientoAdapter;
import com.dashboard.developers.comercio_idolo_java.ui.Adapter.ResumentArrayAdapter;

import android.support.v4.app.Fragment;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Developers on 30/10/2017.
 */

public class FragmentResumen extends Fragment implements ResumenContract.View{
    // UI variables
    private TextView welcome;
    private TextView last_access;
    private ListView summary_list;
    private ArrayList<Object> list;
    private static ResumentArrayAdapter adapter;
    private ArrayList arrayList;
    private GridView gridView;
    private TextView balance;
    private Context context;
    private CoordinatorLayout rootLayoutAndroid;
    private ResumenContract.Presenter pResumen;
    private BottomSheetBehavior mBottomSheetBehavior;
    private RecyclerView mRecyclerViewMovimientos;
    private MovimientoAdapter movimientoAdapterDebit;
    private MovimientoAdapter movimientoAdapterCredit;
    private List<Summary> ListSummaryDebit = new ArrayList<Summary>();
    private List<Summary> ListSummaryCredit = new ArrayList<Summary>();
    CollapsingToolbarLayout collapsingToolbarLayoutAndroid;

    public static List<ResumenTotal> LresumenTotals = new ArrayList<ResumenTotal>();
    public ResumenTotal resmenTotal = new ResumenTotal("10.00","10.00");

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragmentView = inflater.inflate(R.layout.fragment_resumen, container, false);

        balance = (TextView) fragmentView.findViewById(R.id.balance);
        balance.setText(Customer._instance().getBalance());
        //Ger Grill Layout
        pResumen = new ResumenPresenter(this);
        pResumen.resumen(getActivity());
        gridView = (GridView) fragmentView.findViewById(R.id.grid);
        initInstancesGriView(fragmentView);
        initShetResumen(fragmentView);
        initMovimientos(fragmentView);
        return fragmentView;
    }

    private void initMovimientos(View view) {
        mRecyclerViewMovimientos = (RecyclerView) view.findViewById(R.id.movimientos);
    }

    private void initShetResumen(View view) {
        View bottomSheet = view.findViewById( R.id.layout_resumen_total );
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(0);
    }

    private void initInstancesGriView(View v) {
        rootLayoutAndroid = (CoordinatorLayout) v.findViewById(R.id.android_coordinator_layout);
        collapsingToolbarLayoutAndroid = (CollapsingToolbarLayout) v.findViewById(R.id.collapsing_toolbar_android_layout);
        collapsingToolbarLayoutAndroid.setTitle("Material Grid");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onResumenSuccess(String message) {

    }

    @Override
    public void onResumenFailure(String message) {

    }

    @Override
    public void onResumenNetwork(String message) {

    }

    @Override
    public void onFailureTokenExpired(String message) {
        LoginActivity.startIntent(getContext());
    }

    @Override
    public void chargeAmount(String amount) {
        if(amount != null){
            balance.setText("$ " + amount);
        }
    }

    @Override
    public void onSuccessTotal(String debit_amount, String credit_amount) {
        LresumenTotals.clear();
        LresumenTotals.add(new ResumenTotal("Goles Generado"," $ " + debit_amount));
        LresumenTotals.add(new ResumenTotal("Goles Recividos"," $ " + credit_amount));
        gridView.setAdapter(new CustomAndroidGridViewAdapter(getContext(), LresumenTotals));
        gridView.setAdapter(new CustomAndroidGridViewAdapter(getContext(), LresumenTotals));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                if(position == 0){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    mRecyclerViewMovimientos.setAdapter(movimientoAdapterCredit);
                }else if(position == 1){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    mRecyclerViewMovimientos.setAdapter(movimientoAdapterDebit);
                }

            }
        });
    }

    @Override
    public void onSuccessDebit(List<Trasactions> trasactions) {
        Log.v("trsactions", String.valueOf(trasactions));
        if(trasactions.size() > 0){
            ListSummaryDebit.clear();
            for (int i=0; i < trasactions.size(); i++){
                Log.v("transaciotnsDebit",trasactions.get(i).getGol());
                Trasactions transactionDetalle = trasactions.get(i);
                ListSummaryDebit.add( new Summary.Builder(Summary.TYPE_SUMMARY_DEBIT)
                        .name(transactionDetalle.getCom())
                        .description(transactionDetalle.getTip())
                        .amount(transactionDetalle.getVal())
                        .date(transactionDetalle.getFec()).build());
            }
            movimientoAdapterDebit = new MovimientoAdapter(ListSummaryDebit);
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onSuccessCredit(List<Trasactions> trasactions) {
        Log.v("trsactionsCredit", String.valueOf(trasactions));
        if(trasactions.size() > 0){
            ListSummaryCredit.clear();
            for (int i=0; i < trasactions.size(); i++){
                Log.v("transaciotnsDebit",trasactions.get(i).getTip());
                Trasactions transactionDetalle = trasactions.get(i);
                ListSummaryCredit.add( new Summary.Builder(Summary.TYPE_SUMMARY_CREDT)
                        .name(transactionDetalle.getCom())
                        .description(transactionDetalle.getTip())
                        .amount(transactionDetalle.getVal())
                        .date(transactionDetalle.getFec()).build());
            }
            movimientoAdapterCredit = new MovimientoAdapter(ListSummaryCredit);
        }
    }

    @Subscribe
    public void onPushNotificationEvent(PushNotificationEvent pushNotificationEvent) {

    }

}
