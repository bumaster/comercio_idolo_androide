package com.dashboard.developers.comercio_idolo_java.network;


import com.dashboard.developers.comercio_idolo_java.model.MovimientosResponse;
import com.dashboard.developers.comercio_idolo_java.network.request.Cedula;
import com.dashboard.developers.comercio_idolo_java.network.request.RequestGenerar;
import com.dashboard.developers.comercio_idolo_java.network.request.RequestLogin;
import com.dashboard.developers.comercio_idolo_java.network.request.RequestRedimir;
import com.dashboard.developers.comercio_idolo_java.network.response.BalanceResponse;
import com.dashboard.developers.comercio_idolo_java.network.response.BalanceTotal;
import com.dashboard.developers.comercio_idolo_java.network.response.Response;
import com.dashboard.developers.comercio_idolo_java.network.response.LoginResponse;
import com.dashboard.developers.comercio_idolo_java.network.response.ResponseCedula;


import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Developers on 24/10/2017.
 */
public interface ApiService {

    @POST("trade/authentication")
    Observable<LoginResponse> login(@Body RequestLogin Login);

    @POST("trade/debit")
    Observable<Response> recibir(@Body RequestRedimir Redimir, @Header("authorization") String token);

    @POST("trade/credit")
    Observable<Response> generar(@Body RequestGenerar Generar,@Header("authorization") String token);

    @POST("trade/balance")
    Observable<BalanceResponse> resumen(@Header("authorization") String token);

    @POST("trade/totaltransactions")
    Observable<BalanceTotal> totalTransactions(@Header("authorization") String token);

    @GET("transactions/trade/{type}")
    Observable<MovimientosResponse> totalGenerar(@Header("authorization") String token, @Path("type") String type);

    @POST("trade/search-cedula")
    Observable<ResponseCedula> searchCedula(@Header("authorization") String token, @Body Cedula cedula);
}
