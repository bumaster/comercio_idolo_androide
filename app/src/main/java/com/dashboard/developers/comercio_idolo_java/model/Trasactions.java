package com.dashboard.developers.comercio_idolo_java.model;

/**
 * Created by Developers on 27/11/2017.
 */

public class Trasactions {
    String com;
    String dir;
    String tip;
    String fec;
    String val;
    String gol;

    public String getCom() {
        return com;
    }

    public String getDir() {
        return dir;
    }

    public String getTip() {
        return tip;
    }

    public String getFec() {
        return fec;
    }

    public String getVal() {
        return val;
    }

    public String getGol() {
        return gol;
    }
}
