package com.dashboard.developers.comercio_idolo_java.core.login;


import android.app.Activity;
import android.util.Log;

import com.dashboard.developers.comercio_idolo_java.model.Customer;
import com.dashboard.developers.comercio_idolo_java.network.ApiAdapter;
import com.dashboard.developers.comercio_idolo_java.network.ApiService;
import com.dashboard.developers.comercio_idolo_java.network.NetworkError;
import com.dashboard.developers.comercio_idolo_java.network.request.RequestLogin;
import com.dashboard.developers.comercio_idolo_java.network.response.LoginResponse;
import com.dashboard.developers.comercio_idolo_java.utils.ApiError;
import com.dashboard.developers.comercio_idolo_java.utils.ErrorUtils;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class LoginInteractor implements LoginContract.Interactor, Callback<LoginResponse> {

    private LoginContract.OnLoginListener mOnLoginListener;
    private static String LOG_TAG = "responseLogin";
    private  LoginResponse loginResponse;
    private CompositeSubscription subscriptions;



    public LoginInteractor(LoginContract.OnLoginListener onLoginListener) {
        this.mOnLoginListener = onLoginListener;
        this.subscriptions = new CompositeSubscription();
    }

    @Override
    public void perfomanceLoginConection(final Activity activity, String email, String password) {
        RequestLogin requestLogin = new RequestLogin(email,password);
        ApiService client = ApiAdapter.createService(ApiService.class);
        Subscription subscription =  client.login(requestLogin)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(new Func2<Integer, Throwable, Boolean>() {
                    @Override
                    public Boolean call(Integer retryCount, Throwable throwable) {
                        return retryCount < 6 && throwable instanceof SocketTimeoutException;
                    }
                })
                .subscribe(new Subscriber<LoginResponse>() {

                    @Override
                    public void onCompleted() {
                        Log.v("conComplete",">>>>");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.v("errorOn", String.valueOf(e));
                        mOnLoginListener.onFailureNetworking(new NetworkError(e));

                    }

                    @Override
                    public void onNext(LoginResponse loginResponse) {
                        Log.v("onNexr", loginResponse.getToken());
                        Customer._instance().setToken(loginResponse.getToken());
                        mOnLoginListener.onSuccess(loginResponse.getMessage());
                    }
                });

        subscriptions.add(subscription);

    }

    @Override
    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

        if (response.isSuccessful()){
            loginResponse = response.body();
            if(loginResponse.getCode() == 1010){
                mOnLoginListener.onSuccess("Login correcto");
            }else{
                Log.d("error_alla", String.valueOf(response));
                ApiError error = ErrorUtils.parseError(response);
                Log.d("error_loguin_falla", String.valueOf(error.getCode()));
                mOnLoginListener.onFailure(loginResponse.getMessage());
            }

        }else{
            ApiError error = ErrorUtils.parseError(response);
            Log.d("error_loguin_faild", String.valueOf(error.getCode()));
            mOnLoginListener.onFailure(error.getMessage());
        }
    }

    @Override
    public void onFailure(Call<LoginResponse> call, Throwable t) {
        Log.v("requestFa", String.valueOf(t));
        mOnLoginListener.onFailureNetworking(new NetworkError(t));
//        mOnLoginListener.onFailure(t.getMessage());
    }
}
