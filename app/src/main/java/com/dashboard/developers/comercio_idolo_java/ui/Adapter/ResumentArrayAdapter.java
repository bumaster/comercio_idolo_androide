package com.dashboard.developers.comercio_idolo_java.ui.Adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.model.Account;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Developers on 30/10/2017.
 */

public class ResumentArrayAdapter extends ArrayAdapter<Object> {
    private ArrayList<Object> mAccount;
    Context mContext;
    private LayoutInflater mInflater;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtType;
        TextView txtVersion;
        TextView saldo_disponible;
    }

    public ResumentArrayAdapter(@NonNull Context context, ArrayList<Object> Account) {
        super(context, 0,Account);
        mInflater = LayoutInflater.from(context);
        this.mAccount = Account;
        this.mContext = context;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Get the data item for this position
        Account mAccounts = (Account) getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_resumen, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.type);
            viewHolder.txtType = (TextView) convertView.findViewById(R.id.number);
            viewHolder.txtVersion = (TextView) convertView.findViewById(R.id.type2);
            viewHolder.saldo_disponible = (TextView) convertView.findViewById(R.id.disponible_saldo);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
        viewHolder.txtName.setText(mAccounts.getNumber());
        viewHolder.txtType.setText(mAccounts.getType());
        viewHolder.txtVersion.setText(mAccounts.getType());
        viewHolder.saldo_disponible.setText(mAccounts.getType());
        // Return the completed view to render on screen
        return convertView;
    }


}
