package com.dashboard.developers.comercio_idolo_java.ui.Activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.model.Customer;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.logging.Handler;

/**
 * Created by Developers on 31/10/2017.
 */

public class SplashActivity extends AppCompatActivity {
    private static final int SPLASH_TIME_MS = 4000;
    private android.os.Handler mHandler;
    private Runnable mRunnable;
    public static final String PREFS_NAME = "MainActivity";
    String token;
    LinearLayout l1,l2;
    Button btnsub;
    Animation uptodown,downtoup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        bindView();
        runCiclo();
    }

    private void runCiclo() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        token = settings.getString("token",null);
        mHandler = new android.os.Handler();
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        String token2 = FirebaseInstanceId.getInstance().getToken();
        Log.i("FCM Registration Token", "FCM Registration Token: " + token2);
        mRunnable = new Runnable() {
            @Override
            public void run() {
                // otherwise redirect the user to login activity
                Log.v("token_authenticatio","toke:" + token);

                if(!Customer._instance().isAuthentication() && token == null){// entra sino ecuentra token
                    Log.v("customer111 ", String.valueOf(Customer._instance().isAuthentication()));
                    LoginActivity.startIntent(SplashActivity.this);
                }else{
                    Log.v("customer2222 ", String.valueOf(Customer._instance().isAuthentication()));
                    Customer._instance().setToken(token);
                    MainActivity.startIntent(SplashActivity.this);
                }
                finish();
            }
        };

        mHandler.postDelayed(mRunnable, SPLASH_TIME_MS);
    }

    private void bindView() {
        btnsub = (Button)findViewById(R.id.buttonsub);
        l1 = (LinearLayout) findViewById(R.id.l1);
        l2 = (LinearLayout) findViewById(R.id.l2);
        RunAnimation();
    }

    private void RunAnimation() {
        uptodown = AnimationUtils.loadAnimation(this,R.anim.uptodown);
        downtoup = AnimationUtils.loadAnimation(this,R.anim.downtoup);
        l1.setAnimation(uptodown);
        l2.setAnimation(downtoup);
    }
}
