package com.dashboard.developers.comercio_idolo_java.network.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Developers on 23/11/2017.
 */

public class BalanceTotal {

    int code;
    String message;

    @SerializedName("customer")
    Trade customer;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Trade getCustomer() {
        return customer;
    }

    public void setCustomer(Trade customer) {
        this.customer = customer;
    }
}
