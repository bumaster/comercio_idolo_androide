package com.dashboard.developers.comercio_idolo_java.model;

/**
 * Created by Developers on 13/11/2017.
 */

public class Cart {
    String name;
    String carId;
    String description;
    String typel;
    String img;

    public Cart(String name, String carId, String description, String typel, String img) {
        this.name = name;
        this.carId = carId;
        this.description = description;
        this.typel = typel;
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTypel() {
        return typel;
    }

    public void setTypel(String typel) {
        this.typel = typel;
    }
}
