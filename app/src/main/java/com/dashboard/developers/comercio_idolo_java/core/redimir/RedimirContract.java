package com.dashboard.developers.comercio_idolo_java.core.redimir;

import android.app.Activity;

import com.dashboard.developers.comercio_idolo_java.network.NetworkError;
import com.dashboard.developers.comercio_idolo_java.network.response.LoginResponse;

/**
 * Created by Developers on 31/10/2017.
 */

public interface RedimirContract {

    interface View {
        void onRedimeSuccess(String message);

        void onRedimeFailure(String message);

        void onFailureNetwork(String message);

        void onSuccessCedula(String name);

        void onFaildCedula(String message);

    }

    interface Presenter{
        void redime(Activity activity, String amount,String cedula);

        void consultarCedula(Activity activity,String cedula);

    }

    interface Interactor{
        void perfomanceRedimeConection(Activity activity,String amount,String cedula);

        void perfomanceConsultarCedula(Activity activity,String cedula);

        void OnSuscription();

        void OutSuscription();
    }

    interface OnRedimeListener {
        void onSuccess(String message);

        void onFailureNetwork(NetworkError networkError);

        void OnSuscription();

        void OutSuscription();

        void onSuccessCedula(String name);

        void onFaildCedula(NetworkError networkError);
    }
}
