package com.dashboard.developers.comercio_idolo_java.ui.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.model.ResumenTotal;
import com.dashboard.developers.comercio_idolo_java.network.response.Trade;

import java.util.List;

/**
 * Created by Developers on 1/11/2017.
 */

public class CustomAndroidGridViewAdapter extends BaseAdapter {

    private Context mContext;
    private final List<ResumenTotal> resumenTotals;
//    private final int[] Imageid;

    public CustomAndroidGridViewAdapter(Context c,List<ResumenTotal> resumenTotals ) {
        mContext = c;
        this.resumenTotals = resumenTotals;
    }

    @Override
    public int getCount() {
        return resumenTotals.size();
    }

    public void add(ResumenTotal user) {
        resumenTotals.add(user);
//        notifyItemInserted(mUsers.size() - 1);
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.gridview_custom_layout, null);
            TextView textTitle = (TextView) grid.findViewById(R.id.gridview_title);
            TextView textAmount= (TextView) grid.findViewById(R.id.gridview_amount);
            textTitle.setText(resumenTotals.get(i).getTitle());
            textAmount.setText(resumenTotals.get(i).getAmount());
        } else {
            grid = (View) view;
        }

        return grid;
    }
}
