package com.dashboard.developers.comercio_idolo_java.ui.listener;

import android.support.v4.view.ViewPager;

/**
 * Created by Developers on 30/10/2017.
 */
@SuppressWarnings("ALL")
public interface PageIndicator extends ViewPager.OnPageChangeListener {

    void setViewPager(ViewPager view);

    void setViewPager(ViewPager view, int initialPosition);

    void setCurrentItem(int item);

    void setOnPageChangeListener(ViewPager.OnPageChangeListener listener);

    void notifyDataSetChanged();
}
