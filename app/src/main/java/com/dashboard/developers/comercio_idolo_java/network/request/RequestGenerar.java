package com.dashboard.developers.comercio_idolo_java.network.request;

/**
 * Created by Developers on 8/11/2017.
 */

public class RequestGenerar {
    String amount;
    String cedula;
    String amount_transaction;
    String authorization_code;

    public RequestGenerar(String amount, String cedula,String amount_transaction,String transaction_code) {
        this.amount = amount;
        this.cedula = cedula;
        this.amount_transaction = amount_transaction;
        this.authorization_code = transaction_code;
    }
}
