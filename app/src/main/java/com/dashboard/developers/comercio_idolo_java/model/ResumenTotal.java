package com.dashboard.developers.comercio_idolo_java.model;

/**
 * Created by Developers on 23/11/2017.
 */

public class ResumenTotal {
    String title;
    String amount;

    public ResumenTotal(String title, String amount) {
        this.title = title;
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
