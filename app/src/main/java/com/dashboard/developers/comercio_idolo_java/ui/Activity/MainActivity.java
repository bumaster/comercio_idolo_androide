package com.dashboard.developers.comercio_idolo_java.ui.Activity;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.dashboard.developers.comercio_idolo_java.R;
import com.dashboard.developers.comercio_idolo_java.model.Customer;
import com.dashboard.developers.comercio_idolo_java.ui.Fragments.FragmentGenerar;
import com.dashboard.developers.comercio_idolo_java.ui.Fragments.FragmentPayment;
import com.dashboard.developers.comercio_idolo_java.ui.Fragments.FragmentRedimir;
import com.dashboard.developers.comercio_idolo_java.ui.Fragments.FragmentResumen;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private  Toolbar toolbar;
    private TextView mTitle;
    private DrawerLayout drawer;
    public static final String PREFS_NAME = "MainActivity";

    public static void startIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
        init();

    }

    private void init() {
        // set toolbar title
        Fragment fragment = null;
        fragment = new FragmentResumen();
        // set the register screen fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_content,fragment);
        fragmentTransaction.commit();
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar.setTitle("");
        mTitle.setText("Comercio Idolo");
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerStateChanged(int arg0) {
                Log.v("onDrawerStateChanged","onDrawerStateChanged");
            }

            @Override
            public void onDrawerSlide(View arg0, float arg1) {
                Log.v("onDrawerSlide","onDrawerSlide");
            }

            @Override
            public void onDrawerOpened(View arg0) {
                Log.v("onDrawerOpened","onDrawerOpened");
                HideImput();
            }

            @Override
            public void onDrawerClosed(View arg0) {
                Log.v("onDrawerClosed","onDrawerClosed");

            }
        });
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Fragment fragment = null;
        fragment = new FragmentRedimir();
        int id = item.getItemId();

        if (id == R.id.nav_resumen) {          // Handle the camera action
            mTitle.setText("Resumen");
            fragment = new FragmentResumen();

        } else if (id == R.id.nav_redimir) {
            mTitle.setText("Recibir");
            fragment = new FragmentRedimir();
        } else if (id == R.id.nav_generar) {
            mTitle.setText("Generar Goles");
            fragment = new FragmentGenerar();
        } else if (id == R.id.nav_payment) {
            mTitle.setText("");
            fragment = new FragmentPayment();
        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(this, MapsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_cerra_sesion) {
            LoginActivity.startIntent(MainActivity.this);
        }

        // set the register screen fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_content,fragment);
        fragmentTransaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void HideImput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }


    @Override
    protected void onStop() {
        super.onStop();
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        Log.v("OnStopEje","Se ejecuto Onstop");
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("token", Customer._instance().getToken());

        // Commit the edits!
        editor.apply();
    }
}
